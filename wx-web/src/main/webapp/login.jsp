<%--
  Created by IntelliJ IDEA.
  staff: myd
  Date: 2017/7/11
  Time: 10:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <% String path = request.getContextPath(); %>
    <title>登录</title>
    <link rel="stylesheet" href="<%=path%>/static/plugins/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="<%=path%>/static/css/login.css" />
    <script language="JavaScript">
        if (window != top)
        {
            top.location.href = location.href;
        }
    </script>
</head>

<body class="beg-login-bg">
<div class="beg-login-box">
    <header>
        <h1>青海微讯科技有限公司</h1>
    </header>
    <div class="beg-login-main">
        <form id="login_form" class="layui-form">
            <div class="layui-form-item">
                <label class="beg-login-icon">
                    <i class="layui-icon">&#xe612;</i>
                </label>
                <input type="text" id="staff_loginname" name="staff_loginname"  lay-verify="required" autocomplete="off" placeholder="这里输入登录名" class="layui-input">
            </div>
            <div class="layui-form-item">
                <label class="beg-login-icon">
                    <i class="layui-icon">&#xe642;</i>
                </label>
                <input type="password" id="staff_password" name="staff_password"  lay-verify="required" autocomplete="off" placeholder="这里输入密码" class="layui-input">
            </div>
            <div class="layui-form-item">
                <div class="beg-pull-left">
                    <input style="width: 150px;" type="text" id="code_value" name="code_value" lay-verify="required" autocomplete="off" placeholder="这里输入验证码" class="layui-input">
                </div>
                <div class="beg-pull-right">
                    <img  id="image" alt="看不 清，换一张" title="点击重新加载验证码" src="<%=path%>/code" onclick="change_code()"/>
                </div>
                <div class="beg-clear"></div>
            </div>
            <div class="layui-form-item">
                <div class="beg-pull-left beg-login-remember">
                    <label>记住帐号？</label>
                    <input type="checkbox" name="rememberMe" value="true" lay-skin="switch" checked title="记住帐号">
                </div>
                <div class="beg-pull-right">
                    <button class="layui-btn layui-btn-primary" lay-submit lay-filter="login" id="login">
                        <i class="layui-icon">&#xe650;</i> 登录
                    </button>
                </div>
                <div class="beg-clear"></div>
            </div>
        </form>
    </div>
    <footer>
        <p>
                青海微讯科技有限公司
        </p>
    </footer>
</div>
<script type="text/javascript" src="<%=path%>/static/plugins/layui/layui.js"></script>
<script>
    layui.use(['layer', 'form'], function() {
            var layer = layui.layer,
            $ = layui.jquery,
            form = layui.form;

        //刷新验证码
        change_code=function () {
            document.getElementById("image").src="<%=path%>/code?cc="+ Math.random();
        }

        //监听提交
        form.on('submit(login)', function(data){
            var url = "<%=path%>/login";
            $.ajax({
                url:url,
                type: 'GET',
                data:$('#login_form').serialize(),//form id
                datatype:"json",
                cache:false,
                success:function(response){
                    if(response.state=="success")
                    {
                        window.location.href="<%=path%>/loginSuccess";
                    }
                    else
                    {
                        layer.alert(response.msg);
                        change_code();
                    }
                }
            });
            return false;
        });

    });
</script>
</body>

</html>
