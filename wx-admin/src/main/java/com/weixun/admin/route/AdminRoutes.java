package com.weixun.admin.route;

import com.jfinal.config.Routes;
import com.weixun.admin.controller.*;

public class AdminRoutes extends Routes {

    public void config()
    {
        add("/", MainController.class);
        add("/user", UserController.class);
        add("/staff",StaffController.class);
        add("/menu",MenuController.class);
        add("/log",LogController.class);
        add("/role",RoleController.class);
    }

}
